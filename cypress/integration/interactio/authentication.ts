const email_correct: string = 'recruitment_test@interactio.io'
const password_correct: string = 'WelcomeOnBoard!'

// Generating a random string for the email and password
const email_incorrect: string = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '@interactio.io';
const password_incorrect: string = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

const url: string = 'https://dev-panel.interactio.io/login'

describe('Test whether you can authenticate with the correct credentials.', () => {
    it('Visit the dev panel login page.', () => {
        cy.visit(url)
    })

    it('Get the email, password and login elements from the DOM.', () => {
        cy.get('input[type="email"]')
        cy.get('input[type="password"]')
        cy.get('div.button[ng-reflect-klass="button relative"]')
    })

    it('Set the correct credentials and press the login button.', () => {
        cy.get('input[type="email"]').clear().type(email_correct)
        cy.get('input[type="password"]').type(password_correct)
        cy.get('div.button[ng-reflect-klass="button relative"]').click()
    })

    it('Check if the login attempt was succesful.', () => {
        cy.url().should('eq', 'https://dev-panel.interactio.io/dashboard')
        cy.get('.colored').as('invalid_cred').then(($cred) => {
            expect($cred).to.be.hidden
        })
    })
})

describe('Test whether you cannot authenticate with the incorrect credentials', () => {
    it('Visit the dev panel login page.', () => {
        cy.visit(url)
    })

    it('Get the email, password and login elements from the DOM.', () => {
        cy.get('input[type="email"]')
        cy.get('input[type="password"]')
        cy.get('div.button[ng-reflect-klass="button relative"]')
    })

    it('Set the incorrect credentials and press the login button.', () => {
        cy.get('input[type="email"]').clear().type(email_incorrect)
        cy.get('input[type="password"]').type(password_incorrect)
        cy.get('div.button[ng-reflect-klass="button relative"]').click()
    })

    it('Check whether the login attempt failed.', () => {
        cy.url().should('eq', 'https://dev-panel.interactio.io/login')
        cy.get('.colored').as('invalid_cred').then(($cred) => {
            expect($cred).not.to.be.hidden
        })
    })
})
